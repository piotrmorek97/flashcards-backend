package zti.flashcards.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import zti.flashcards.model.Flashcard;

/**
 * 
 * FlashcardRepository extends JpaRepository interface.
 *
 */
@Repository
public interface FlashcardRepository extends JpaRepository<Flashcard, Integer>{
	/**
	 * 
	 * @return List of Flashcards order by id, ascending.
	 */
	public List<Flashcard> findAllByOrderByIdAsc();
}
