DROP TABLE IF EXISTS flashcards;
CREATE TABLE flashcards (id SERIAL PRIMARY KEY, term VARCHAR, definition VARCHAR);

INSERT INTO flashcards (term, definition) VALUES ('pies', 'dog');
INSERT INTO flashcards (term, definition) VALUES ('kot', 'cat');
INSERT INTO flashcards (term, definition) VALUES ('Dostałem psa wczoraj', 'Yesterday I got a dog');
