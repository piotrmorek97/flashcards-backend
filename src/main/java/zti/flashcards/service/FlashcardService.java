package zti.flashcards.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zti.flashcards.model.Flashcard;
import zti.flashcards.repository.FlashcardRepository;

/**
 * 
 * FlashcardService class
 *
 */
@Service
public class FlashcardService {

	/**
	 * DI for FlashcardRepository
	 */
	@Autowired
	private FlashcardRepository flashcardRepo;
	
	/**
	 * Get all Flashcards ordered by id
	 * @return Flashcards ordered by id
	 */
	public List<Flashcard> getAllFlashcards(){
		return flashcardRepo.findAllByOrderByIdAsc();
	}
	
	/**
	 * Get Flashcard by id
	 * @param id
	 * @return Flashcard
	 */
	public Optional<Flashcard> getFlashcardById(Integer id) {
		return flashcardRepo.findById(id);
	}
	
	/**
	 * Add new Flashcard
	 * @param flashcard
	 */
	public void addFlashcard(Flashcard flashcard) {
		flashcardRepo.save(flashcard);
	}
	
	/**
	 * Delete Flashcard by id
	 * @param id
	 */
	public void deleteFlashcard(Integer id) {
		flashcardRepo.deleteById(id);
	}
	
	/**
	 * Updating Flashcard. If Flashcard with input id will be not find in DB, 
	 * new Flashcard is saved. Otherwise new Flashcard object is created and saved.
	 * @param id
	 * @param flashcard
	 */
	public void updateFlashcard(Integer id, Flashcard flashcard) {
		Optional<Flashcard> flashcardFromDb = flashcardRepo.findById(id);
		if(flashcardFromDb.isEmpty()) {
			flashcardRepo.save(flashcard);
		} else {
			Flashcard newFlashcard = new Flashcard();
			newFlashcard.setId(id);
			newFlashcard.setTerm(flashcard.getTerm());
			newFlashcard.setDefinition(flashcard.getDefinition());
			flashcardRepo.save(newFlashcard);
		}
	}
}
