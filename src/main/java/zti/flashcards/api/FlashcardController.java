package zti.flashcards.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import zti.flashcards.model.Flashcard;
import zti.flashcards.service.FlashcardService;

/**
 * FlashcardController class
 * Rest Controller with endpoints
 * GET, POST, PUT and DELETE
 *
 */

@RequestMapping("api/v1/flashcards")
@RestController
@CrossOrigin("*")
public class FlashcardController {

	/**
	 *  Dependency Injection, FlashcardService
	 */
	@Autowired
	private FlashcardService flashcardService;
	
	/**
	 * Get all Flashcards
	 * @return List of Flashcards
	 */
	@GetMapping
	public @ResponseBody List<Flashcard> getFlashcards(){
		return flashcardService.getAllFlashcards();
	}
	
	/**
	 * Get Flashcard by id
	 * @param id
	 * @return Flashcard by id
	 */
	@GetMapping(path = "{id}")
	public @ResponseBody Optional<Flashcard> getFlashcardById(@PathVariable("id") Integer id){
		return flashcardService.getFlashcardById(id);
	}
	
	/**
	 * Post new Flashcard
	 * @param newFlashcard
	 */
	@PostMapping
	public void addFlashcard(@RequestBody Flashcard newFlashcard) {
		flashcardService.addFlashcard(newFlashcard);
	}
	
	/**
	 * Delete Flashcard
	 * @param id
	 */
	@DeleteMapping(path = "{id}")
	public void deleteFlashcard(@PathVariable("id") Integer id) {
		flashcardService.deleteFlashcard(id);
	}
	
	/**
	 * Update Flashcard
	 * @param id
	 * @param flashcard
	 */
	@PutMapping(path = "{id}")
	public void updateFlashcard(@PathVariable("id") Integer id, @RequestBody Flashcard flashcard) {
		flashcardService.updateFlashcard(id, flashcard);
	}
	
}
